\documentclass[10pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[toc,page]{appendix}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{graphicx}
\usepackage{float}
\usepackage{hyperref}
\usepackage{dsfont}
\usepackage{subcaption}
\usepackage[format=plain,font=small,textfont=it]{caption}
\usepackage{color}
\usepackage{multirow} 
\usepackage{bookmark}
\usepackage{mathrsfs}

% Show labels in formulas, sections etc
%\usepackage{showlabels}

% TODO notes
\usepackage[colorinlistoftodos,prependcaption,textsize=tiny]{todonotes}
\newcommand{\notaFra}[1]{\todo[linecolor=red,backgroundcolor=red!15,bordercolor=red!80,]{Fra: #1}}
\newcommand{\notaPietro}[1]{\todo[linecolor=blue,backgroundcolor=blue!15,bordercolor=blue!80,]{Pietro: #1}}
\newcommand{\notaGio}[1]{\todo[linecolor=purple,backgroundcolor=purple!15,bordercolor=purple!80,]{Giorgio: #1}}

%%%%%%%%% FORMATTING %%%%%%%%%%
\setlength{\topmargin}{0cm}
\setlength{\textheight}{23cm}
\setlength{\textwidth}{16.5cm}
\setlength{\evensidemargin}{-0.75cm}
\setlength{\oddsidemargin}{-0.75cm}
\addtolength{\voffset}{-1.5cm}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%% Scalar products %%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\ket}[1]{\left|#1\right\rangle}
\newcommand{\bra}[1]{\left\langle #1\right|}
\newcommand{\ra}{\rangle}
\newcommand{\scal}[1]{\left\langle #1\right\rangle}
\newcommand{\Tr}{\mathrm{Tr}}
\newcommand{\nn}{\nonumber}


\newcommand{\Wthree}[6]{\left(\begin{array}{ccc} #1 & #2 & #3 \\ #4 & #5 & #6 \end{array}\right)}
\newcommand{\Wfour}[9]{\left(\begin{array}{cccc} #1 & #2 & #3 & #4 \\ #5 & #6 & #7 & #8 \end{array}\right)^{(#9)}}
\newcommand{\Wsix}[6]{\left \{ \begin{array}{ccc} #1 & #2 & #3 \\ #4 & #5 & #6 \end{array}\right \} }
\newcommand{\Wnine}[9]{\left \{ \begin{array}{ccc} #1 & #2 & #3  \\ #4 & #5 & #6 \\ #7 & #8 & #9 \end{array}\right \} }


\newcommand{\comment}[1]{{\color{red}#1}}
\newcommand{\coolname}{\texttt{sl2cfoam}}


\begin{document}

\title{La Scienza ai tempi del Corona}

\author{\Large{Pietro Don\`a\footnote{dona@cpt.univ-mrs.fr}, \ } \Large{Francesco Gozzini\footnote{gozzini@cpt.univ-mrs.fr}, \ } \Large{Giorgio Sarno\footnote{sarno@cpt.univ-mrs.fr} \ }
\smallskip \\ 
\small{CPT, Aix-Marseille\,Universit\'e, Universit\'e\,de\,Toulon, CNRS, 13288 Marseille, France}
}

\date{\today}

\maketitle

\begin{abstract}
\noindent 
\end{abstract}
    

\section{Introduction and Motivations}
\label{sec:intro}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Four dimensional spin foam BF theory}
\label{sec:theory}

General Relativity in four dimensions can be written as a BF theory with constraints. This is the starting point of many spin foam models, as the Lorentzian EPRL model. Therefore it is important to study BF theory's properties and to unveil some of its features. A detailed overview of BF theory and its relations with spin foam model can be found in \cite{} \cite{}.
In particular we study the case of a BF theory with $G=SU(2)$ compact Lie group and a four dimensional manifold $\machcal{M}$. 
Classically, BF action is given by 
\begin{equation}
  S[B,\omega] = \int_{\mathcal{M}}Tr B \wedge F(\omega)
\end{equation}
where B is 2-form with values in the $\machcal{su(2)}$ algebra, $\omega$ a connection on a SU(2) principal bundle over $\mathcal{M}$ and F its curvature. BF theory is topological, it has no local degrees of freedom and all solutions of the equations of motion are locally related by gauge transformations \cite{}.
Formally, we can write the partition function as
%
\begin{equation}
  \label{eq:partfunct1}
      \mathcal{Z} = \int \mathcal{D}[B]\mathcal{D}[\omega] \exp(i \int_{\mathcal{M}}Tr(B \wedge F(\omega)) \ ,
  \end{equation}
%
Integrating over the B field we get
 %
\begin{equation}
  \mathcal{Z} = \int \mathcal{D}[\omega] \delta(F(\omega)) \ .
\end{equation}
% 
This expression is formal. To make this formula concrete we need to discretize the manifold with a triangulation $\Delta$. It defines a two-complex $\Delta^\star$ that consists in a set of vertices (dual to 4-simplices in the actual traingulation) edges (dual to tetrahedra) and faces (dual to triangles). We discretize the connection $\omega$ with group elements $g_e$ assigned to edges of $\Delta^\star$ and we write the discrete partition function as
%
\begin{equation}
  \mathcal{Z}(\Delta) = \int \prod_e dg_e \prod_f \delta(g_{e_1} \dots g_{e_n}) \ , \qquad \text{ with } e_i \subset f
\end{equation}
%
with the product $g_{e_1} \dots g_{e_n}$ representing the holonomy around a single face $f$ of $\Delta^*$, $dg_e$ is the Haar measure on $SU(2)$ and $\delta$ is the Dirac delta function on $SU(2)$. We can now use the Peter Weyl theorem to expand the delta function as $\delta(g) = \sum_j (2j+1) Tr(D^j(g))$, where $D^j(g)$ are the Wigner functions. We get
%
\begin{align}
  \label{eq:graphical}
      \mathcal{Z}(\Delta) &= \sum_{j_f} \int \prod_e dg_e \prod_f (2 j_f+1) Tr (D^{j_f}(g_{e_1} \dots g_{e_n})) \\
      & = \sum_{j_f}  \prod_f (2 j_f +1)  \prod_v \raisebox{-10mm}{ \includegraphics[width=4cm]{vertex_box.eps}} \ .
  \end{align}
  %
Here we introduced, for spin foam vertices, a graphical notation. Every box represents an integral over SU(2), each strand is a representation of SU(2). For more details on graphical notations see \cite{} \cite{}. Using this we can organize the partition function as a product of vertices of $\Delta^\star$, every vertex is related to a 4-simplex in $\Delta$. The connection between vertices is given by the connectivity of the triangulation.
In \ref{eq:graphical} we can explicitly perform the integrations over SU(2). This allows to rewrite $\mathcal{Z}$ as
%
\begin{equation}
  \mathcal{Z}(\Delta)  = \sum_{j_f}  \prod_f A_f \prod_e A_e \prod_v A_v
\end{equation}
%
In the SU(2) four dimensional case we get $A_e =1$ trivial edge amplitude, $A_f = 2j_f +1$ face amplitude, and a vertex amplitude $A_v$ given by a $\{15j\}$ symbol.
%
\begin{equation}
  A_v = \left \{ \begin{array}{ccccc} j_1 & j_2 & j_3 & j_4 & j_5 \\ k_1 & k_2 & k_3 & k_4 & k_5  \\ l_1 & l_2 & l_3 & l_4 & l_5  \end{array}\right \}  = \raisebox{-15mm}{ \includegraphics[width=3.5cm]{15j_Yutsis.eps}} \ .
\end{equation}
%
where we follow the conventions given in \cite{Yutsis}.
The SU(2) BF model has been studied analytically in \cite{} and numerically in \cite{}. It is an interesting playground to study properties of spin foam models. In fact the asymptotic limit of the vertex amplitude, when contracted with appropriate coherent states, is related to the four dimensional Regge Action of a single 4-simplex.

Mettiamo la formula asintotica qui per 4-simplex? 

Moreover, as suggested in \cite{}, it can be used as a toy model to discover original features of spin foam models, as the EPRL. While BF models are topological the EPRL model aims to be an appropriate quantum discretization of four dimensional General Relativity, as the Ponzano-Regge model is for three dimensional General Relativity. It is interesting to study, numerically or analitically, where differences between the two theories originate.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{The Delta 3 amplitude}
\label{sec:d3theory}

In this paper we want to study the stationary phase point of a particular traingulation formed by three 4-simplices glued together along a triangle. As done in the three dimensional case in \cite{}, using the Ponzano Regge model, we call this amplitude $\Delta_3$.
In Figure \ref{} we show its dual 2-complex: the three 4- simplices are represented by three vertices. They have one one triangle in common represented by a bulk face. This triangulation has nine boundary tetrahedra, they are represented using Livine-Speziale coherent states. Each boundary strand (dual to a triangle) is rapresented by a spin $j_i$, there are 18 of them, while the bulk face is called x.  We study in the next section the geometrical reconstruction needed to construct appropriate boundary data. 

Mettere immagine che dovrebbe avere fra con la connettività tra vertici? O qui o nella ricostruzione geometrica.

\begin{figure}[H]
  \centering
  \begin{subfigure}[c]{4cm}
      \includegraphics[width=4cm]{}
  \end{subfigure}
  \hspace{1cm}
  \begin{subfigure}[c]{0.49\textwidth}
      \includegraphics[width=7cm]{triangulation_amp.eps}
  \end{subfigure}
  \caption{Left: The 2-complex dual to the $\Delta_3$ triangulation. In red the bulk face.  Right: The spin foam diagram associated to the $\Delta_3$ transition amplitude. The boxes represent integrals over the $SU(2)$ group, while strand representations labeled by a spin. The empty dots on the boundary are coherent states, described in Section \ref{}. }
  \label{fig:spinfoam}
\end{figure}

Altro punto delicato: prima di dare la forma esplicita bisogna parlare degli stati coerenti se no non si capisce. 
E poi, nell'immagine mettiamo già tutti i numerelli oppure questa cosa la lasciamo completamente nell'appendice?
Eventualmente nel paragrafo precedente dove definiamo il 15j si può definire direttamente con delle convenzioni simili a quelle che usiamo poi qui.

In appendix \ref{} we compute explicitly the transition amplitude. The result is the following
\begin{align}  
  \label{eq:d3ampl}
      W_{\Delta_3} (j_f, n_f) & = (-1)^{\chi} \sum_x (2 x +1)\sum_{i_a} \sum_{k_b} (2 k_b + 1) (-1)^{x+k_1+k_2+k_3} ( \prod_a (2 i_a + 1) c_{i_a} (\vec{n_f}) ) \\ & \times \left \{ \begin{array}{ccccc} i_1 & j_{345} & k_1 & j_{145} & i_2 \\ j_{125} & k_2 & j_{123} & i_3 & j_{234}  \\ j_{235} & x & j_{134} & j_{124} & j_{245}  \end{array}\right \} \left \{ \begin{array}{ccccc} i_4 & j_{123} & k_2 & j_{235} & i_5 \\ j_{356} & k_3 & j_{156} & i_6 & j_{126}  \\ j_{136} & x & j_{125} & j_{256} & j_{236}  \end{array}\right \} \\ & \times \left \{ \begin{array}{ccccc} i_7 & j_{156} & k_3 & j_{136} & i_8 \\ j_{134} & k_1 & j_{345} & i_9 & j_{456}  \\ j_{145} & x & j_{356} & j_{346} & j_{146}  \end{array}\right \} 
\end{align}
%
where $\chi = 2 (j_{123} + j_{234}+ j_{124}+ j_{134}+j_{456}+j_{156}+j_{346}+j_{356}+j_{235}) + j_{123} + j_{345} + j_{156}$ is a global sign, $i_a$ are boundary intertwiners, $k_b$ internal intertwiners and the sum over x is bounded by triangular inequalities.$c_{i_a} (\vec{n_f})$ are Livine Speziale coherent intertwiners, they assign a normal $n_f$ at each strand. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{The Delta 3 geometrical reconstruction}
\label{sec:d3geometry}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Numerical method}
\label{sec:numericalmet}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Numerical analysys}
\label{sec:numericalan}

We want to numerically investigate the stationary phase points of the $\Delta_3$ amplitude for the previously described boundary data.
We developed a C-based code using the libraries \texttt{wigxjpf} \cite{Johansson:2015cca} and \texttt{sl2cfoam} \cite{Dona:2018nev}. The code developed using this tools is available at \cite{}. 
Computations are performed using the servers available at CPT, namely DETTAGLI SERVER. While in \cite{} computations were performed in minutes or seconds on a personal computer here they are slower. Depending on the configuration chosen and on the rescaling parameter considered these numerical tasks require hours of computational time on a server. We will give more details in the following.

We want to use the techniques developed in \cite{} to ask the following question: is the summation over the internal face dominated by some specific value of x? The first configuration we study is the flat one. The boundary spins are all fixed to be equal $j_i=\lambda$ while the normals for the coherent states are fixed following \ref{{sec:d3geometry}. In \ref{fig:partial_flat} we show the partial sum for this configuration at $\lambda = 30$. We present with a red line the geometrical value obtained classicaly while with a dashed line we enlight the result of out numerical analysis and alghoritm.

\begin{figure}[H]
  \centering
  \includegraphics[width=10.5cm]{eqconfig2_60.pdf}
  \caption{}
  \label{fig:partial_flat}
\end{figure}
Using the alghoritm described in the previous section and its implementation as a Mathematica notebook (avalaible in \cite{}) we get the following the result for the numerical stationary phase point
\begin{equation}
  \label{eq:flatresult}
  x_t =\lambda  \approx  \qquad x_n= \pm
  \end{equation}
We find agreement with the geometrical value.

We perform the same analysis on two configurations embedded in a curved spacetime. 

\begin{figure}[H]
  \centering
  \includegraphics[width=10.5cm]{curvyconfig1_40.pdf}
  \caption{}
  \label{fig:partial_curv1}
\end{figure}

\begin{equation}
  \label{eq:curvy1result}
  x_t =\lambda  \approx  \qquad x_n= \pm
  \end{equation}

\begin{figure}[H]
  \centering
  \includegraphics[width=10.5cm]{curvyconfig2_40.pdf}
  \caption{}
  \label{fig:partial_curv2}
\end{figure}

\begin{equation}
  \label{eq:curvy2result}
  x_t =\lambda  \approx  \qquad x_n= \pm
  \end{equation}

We summarize the results in Table \ref{}

\begin{center}
  \begin{tabular}{c|c|c|}
   flat-config & numerical & analytic \tabularnewline
   & $x$ & $x$ \tabularnewline
  \hline 
  $\lambda=24:$ & $34\pm3$ &$81.9$\tabularnewline
  \hline 
  $\lambda=32:$ & $34\pm3$ &$81.9$\tabularnewline
  \hline 
  $\lambda=40:$ & $45\pm2$ & $109.2$\tabularnewline
  \hline 
  $\lambda=60:$ & $67\pm2$ & $163.9$\tabularnewline
\end{tabular}
\end{center}

\begin{center}
  \begin{tabular}{c|c|c|}
    curv-config-1  & numerical & analytic \tabularnewline
   & $x$ & $x$ \tabularnewline
  \hline 
  $\lambda=24:$ & $34\pm3$ &$81.9$\tabularnewline
  \hline 
  $\lambda=32:$ & $45\pm2$ & $109.2$\tabularnewline
  \hline 
  $\lambda=40:$ & $57\pm2$ $& $136.6$\tabularnewline
 \end{tabular}
\end{center}

\begin{center}
  \begin{tabular}{c|c|c|}
    curv-config-2  & numerical & analytic \tabularnewline
   & $x$ & $x$ \tabularnewline
  \hline 
  $\lambda=24:$ & $34\pm3$ &$81.9$\tabularnewline
  \hline 
  $\lambda=32:$ & $45\pm2$ & $109.2$\tabularnewline
  \hline 
  $\lambda=40:$ & $57\pm2$ $& $136.6$\tabularnewline
 \end{tabular}
\end{center}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Conclusion and outlook}
\label{sec:concl}


\begin{appendices}
\section{Derivation of $\Delta_3$ spinfoam amplitude} 
\label{app:derivation}

In this appendix we derive the formula for the four dimensional spin foam associated with the $\Delta_3$ triangulation.
The transition amplitude is given by
%
\begin{equation}
  W_{\Delta_3} (j_f,\vec{n}_f) = \sum_x (2x + 1) \raisebox{-30 mm}{ \includegraphics[width=8cm]{triangulation_amp_label.eps}}
\end{equation}
%
where we have chosen a convention for the direction of the strands. The 4-simplices are labeled by five number representing their five vertices while the boxes (namely the tetrahedra in the dual picture) by four numbers and the strands are labeled by three numbers. In red we enlight the internal face $x$. On the boundary we represent coherent states as an empty dot. 
We can perform the integration over $SU(2)$ and we get
%
\begin{align}
  W_{\Delta_3} (j_f,\vec{n}_f) & =   (-1)^{\chi_1}\sum_x (2 x +1)\sum_{i_a} \sum_{k_b} (2 k_b + 1)^2 ( \prod_a (2 i_a + 1) c_{i_a}(\vec{n_f})) \\ \times & \raisebox{-20 mm}{ \includegraphics[width=9cm]{computation_amp1.eps}}
\end{align}
%
where $\chi_ = 2 (j_{123} + j_{234}+ j_{124}+ j_{134}+j_{456}+j_{156}+j_{346}+j_{356}+j_{235})$ is a global sign that depends on the orientations chosen for the strands. The coherent tetrahedra are called $c_{i_a}(\vec{n_f}$ and we avoid using their graphical representation to keep the image as simple as possible. 
We compute the phase
%
\begin{equation}
 \raisebox{-6.2 mm}{ \includegraphics[width=2cm]{phase.eps}} = \frac{(-1)^{k + j_1 +j_2}} {2k + 1}
\end{equation}
%
and we automatically get the $\Delta_3$ transition amplitude in terms of three $\{15j\}$ symbols
%
\begin{align}
  W_{\Delta_3} (j_f,\vec{n}_f) &=  (-1)^{\chi} \sum_x (2 x +1)\sum_{i_a} \sum_{k_b} (2 k_b + 1) (-1)^{x+k_1+k_2+k_3} ( \prod_a (2 i_a + 1) c_{i_a}(\vec{n_f})) \\ & \times \raisebox{-20 mm}{ \includegraphics[width=12cm]{computation_amp2.eps}}
\end{align}
%
Here $\chi = 2 (j_{123} + j_{234}+ j_{124}+ j_{134}+j_{456}+j_{156}+j_{346}+j_{356}+j_{235}) + j_{123} + j_{345} + j_{156}$.
\end{appendices}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{thebibliography}{100}

	\bibitem{Engle:2007wy} J.~Engle, E.~Livine, R.~Pereira and C.~Rovelli,
    ``LQG vertex with finite Immirzi parameter,''
    Nucl.\ Phys.\ B {\bf 799}, 136 (2008)
    doi:10.1016/j.nuclphysb.2008.02.018
    [arXiv:0711.0146 [gr-qc]].

	\bibitem{Livine:2007vk} E.~R.~Livine and S.~Speziale,
    ``A New spinfoam vertex for quantum gravity,''
    Phys.\ Rev.\ D {\bf 76}, 084028 (2007)
    doi:10.1103/PhysRevD.76.084028
    [arXiv:0705.0674 [gr-qc]].

	\bibitem{Livine:2007ya} E.~R.~Livine and S.~Speziale,
  ``Consistently Solving the Simplicity Constraints for Spinfoam Quantum Gravity,''
  EPL {\bf 81}, no. 5, 50004 (2008)
  doi:10.1209/0295-5075/81/50004
  [arXiv:0708.1915 [gr-qc]].

	\bibitem{Freidel:2007py} L.~Freidel and K.~Krasnov,
  ``A New Spin Foam Model for 4d Gravity,''
  Class.\ Quant.\ Grav.\  {\bf 25}, 125018 (2008)
  doi:10.1088/0264-9381/25/12/125018
  [arXiv:0708.1595 [gr-qc]].

	\bibitem{Barrett:2009gg} J.~W.~Barrett, R.~J.~Dowdall, W.~J.~Fairbairn, H.~Gomes and F.~Hellmann,
  ``Asymptotic analysis of the EPRL four-simplex amplitude,''
  J.\ Math.\ Phys.\  {\bf 50}, 112504 (2009)
  doi:10.1063/1.3244218
  [arXiv:0902.1170 [gr-qc]].

	\bibitem{Barrett:2011xa} J.~W.~Barrett, R.~J.~Dowdall, W.~J.~Fairbairn, F.~Hellmann and R.~Pereira,
  ``Asymptotic analysis of lorentzian spin foam models,''
  PoS QGQGS {\bf 2011}, 009 (2011).
  doi:10.22323/1.140.0009

	\bibitem{Bianchi:2006uf} E.~Bianchi, L.~Modesto, C.~Rovelli and S.~Speziale,
  ``Graviton propagator in loop quantum gravity,''
  Class.\ Quant.\ Grav.\  {\bf 23}, 6989 (2006)
  doi:10.1088/0264-9381/23/23/024
  [gr-qc/0604044].

	\bibitem{Speziale:2008uw} S.~Speziale,
  ``Background-free propagation in loop quantum gravity,''
  Adv.\ Sci.\ Lett.\  {\bf 2}, 280 (2009)
  doi:10.1166/asl.2009.1036
  [arXiv:0810.1978 [gr-qc]].

	\bibitem{Bianchi:2009ri} E.~Bianchi, E.~Magliaro and C.~Perini,
  ``LQG propagator from the new spin foams,''
  Nucl.\ Phys.\ B {\bf 822}, 245 (2009)
  doi:10.1016/j.nuclphysb.2009.07.016
  [arXiv:0905.4082 [gr-qc]].

	\bibitem{Bianchi:2011hp} E.~Bianchi and Y.~Ding,
  ``lorentzian spinfoam propagator,''
  Phys.\ Rev.\ D {\bf 86}, 104040 (2012)
  doi:10.1103/PhysRevD.86.104040
  [arXiv:1109.6538 [gr-qc]].

	\bibitem{Christodoulou:2016vny} M.~Christodoulou, C.~Rovelli, S.~Speziale and I.~Vilensky,
  ``Planck star tunneling time: An astrophysically relevant observable from background-free quantum gravity,''
  Phys.\ Rev.\ D {\bf 94}, no. 8, 084035 (2016)
  doi:10.1103/PhysRevD.94.084035
  [arXiv:1605.05268 [gr-qc]].

	\bibitem{Bianchi:2010zs} E.~Bianchi, C.~Rovelli and F.~Vidotto,
  ``Towards Spinfoam Cosmology,''
  Phys.\ Rev.\ D {\bf 82}, 084035 (2010)
  doi:10.1103/PhysRevD.82.084035
  [arXiv:1003.3483 [gr-qc]].

	\bibitem{Vidotto:2011qa} F.~Vidotto,
  ``Many-nodes/many-links spinfoam: the homogeneous and isotropic case,''
  Class.\ Quant.\ Grav.\  {\bf 28}, 245005 (2011)
  doi:10.1088/0264-9381/28/24/245005
  [arXiv:1107.2633 [gr-qc]].

	\bibitem{Sarno:2018ses} G.~Sarno, S.~Speziale and G.~V.~Stagno,
  ``2-vertex lorentzian Spin Foam Amplitudes for Dipole Transitions,''
  Gen.\ Rel.\ Grav.\  {\bf 50}, no. 4, 43 (2018)
  doi:10.1007/s10714-018-2360-x
  [arXiv:1801.03771 [gr-qc]].

	\bibitem{Gozzini:2019nbo} F.~Gozzini and F.~Vidotto,
  ``Primordial fluctuations from quantum gravity,''
  arXiv:1906.02211 [gr-qc].

	\bibitem{Dona:2019dkf} P.~Dona, M.~Fanizza, G.~Sarno and S.~Speziale,
  ``Numerical study of the lorentzian EPRL spin foam amplitude,''
  arXiv:1903.12624 [gr-qc].

	\bibitem{Bahr:2014qza} B.~Bahr,
  ``On background-independent renormalization of spin foam models,''
  Class.\ Quant.\ Grav.\  {\bf 34}, no. 7, 075001 (2017)
  doi:10.1088/1361-6382/aa5e13
  [arXiv:1407.7746 [gr-qc]].

	\bibitem{Bahr:2016hwc} B.~Bahr and S.~Steinhaus,
  ``Numerical evidence for a phase transition in 4d spin foam quantum gravity,''
  Phys.\ Rev.\ Lett.\  {\bf 117}, no. 14, 141302 (2016)
  doi:10.1103/PhysRevLett.117.141302
  [arXiv:1605.07649 [gr-qc]].

	\bibitem{Bahr:2017klw} B.~Bahr and S.~Steinhaus,
  ``Hypercuboidal renormalization in spin foam quantum gravity,''
  Phys.\ Rev.\ D {\bf 95}, no. 12, 126006 (2017)
  doi:10.1103/PhysRevD.95.126006
  [arXiv:1701.02311 [gr-qc]].

	\bibitem{Bahr:2017eyi} B.~Bahr, S.~Kloser and G.~Rabuffo,
  ``Towards a Cosmological subsector of Spin Foam Quantum Gravity,''
  Phys.\ Rev.\ D {\bf 96}, no. 8, 086009 (2017)
  doi:10.1103/PhysRevD.96.086009
  [arXiv:1704.03691 [gr-qc]].

  \bibitem{Bahr:2018gwf} B.~Bahr, G.~Rabuffo and S.~Steinhaus,
  ``Renormalization of symmetry restricted spin foam models with curvature in the asymptotic regime,''
  Phys.\ Rev.\ D {\bf 98}, no. 10, 106026 (2018)
  doi:10.1103/PhysRevD.98.106026
  [arXiv:1804.00023 [gr-qc]].

	\bibitem{Mielczarek:2018jsh} J.~Mielczarek,
  ``Spin Foam Vertex Amplitudes on Quantum Computer -- Preliminary Results,''
  doi:10.3390/universe5080179
  arXiv:1810.07100 [gr-qc].

	\bibitem{Encyclopedia} The repositories for the ``Encyclopedia of Quantum Geometries'' can be found at the address \texttt{zenodo.org/communities/enqugeo}.

	\bibitem{Conrady:2008mk} F.~Conrady and L.~Freidel,
  ``On the semiclassical limit of 4d spin foam models,''
  Phys.\ Rev.\ D {\bf 78}, 104023 (2008)
  doi:10.1103/PhysRevD.78.104023
  [arXiv:0809.2280 [gr-qc]].

	\bibitem{Bonzom:2009hw} V.~Bonzom,
  ``Spin foam models for quantum gravity from lattice path integrals,''
  Phys.\ Rev.\ D {\bf 80}, 064028 (2009)
  doi:10.1103/PhysRevD.80.064028
  [arXiv:0905.1501 [gr-qc]].

	\bibitem{Hellmann:2013gva} F.~Hellmann and W.~Kaminski,
  ``Holonomy spin foam models: Asymptotic geometry of the partition function,''
  JHEP {\bf 1310}, 165 (2013)
  doi:10.1007/JHEP10(2013)165
  [arXiv:1307.1679 [gr-qc]].

	\bibitem{Dona:2018nev} P.~Dona and G.~Sarno,
  ``Numerical methods for EPRL spin foam transition amplitudes and lorentzian recoupling theory,''
  Gen.\ Rel.\ Grav.\  {\bf 50}, 127 (2018)
  doi:10.1007/s10714-018-2452-7
  [arXiv:1807.03066 [gr-qc]].

	\bibitem{PonzanoRegge} G.~Ponzano, T.~Regge,
  ``Semiclassical limit of Racah coefficients,''
  Spectroscopic and group theoretical methods in physics (1968), 1-58 pp.

\bibitem{Boosting}
S.~Speziale, {\it {Boosting Wigner's nj-symbols}},  J. Math. Phys. {\bf 58}
  (2017), no.~3 032501 [\href{http://arXiv.org/abs/1609.01632}{{\tt
  1609.01632}}].


	\bibitem{Johansson:2015cca} H.~T. Johansson and C.~Forss{\'e}n, ``Fast and accurate evaluation of wigner
  3j, 6j, and 9j symbols using prime factorisation and multi-word integer
  arithmetic,''  SIAM J. Sci. Statist. Comput. {\bf 38} (2016) A376--A384

	\bibitem{codes} At the address \texttt{ bitbucket.org/giorgiosarno/ponzanoregge\_delta3 } there are the Mathematica's notebooks to perform 
  the geometrical reconstruction for the $\Delta_3$ amplitude. 
  The C-codes we used to compute the bulk distribution are also available.

	\bibitem{Schulman:1981vu} L.~S.~Schulman,
  ``Techniques And Applications Of Path Integration,''
  New York, Usa: Wiley ( 1981) 358p

	\bibitem{Sakurai} J.~J.~Sakurai,
  ``Modern Quantum Mechanics,''
   ,Addison Wesley (1993) 500p

	\bibitem{Baez:1999sr} J.~C.~Baez,
   ``An Introduction to spin foam models of quantum gravity and BF theory,''
   Lect.\ Notes Phys.\  {\bf 543}, 25 (2000)
   doi:10.1007/3-540-46552-9
   [gr-qc/9905087].

	\bibitem{Perez:2012wv} A. ~Perez,
   ``The Spin Foam Approach to Quantum Gravity,''
   Living Rev.\ Rel.\  {\bf 16} (2013) 3
   doi:10.12942/lrr-2013-3
   [arXiv:1205.2019 [gr-qc]].

	\bibitem{Varshalovich} D.~A. Varshalovich, A.~N. Moskalev and V.~K. Khersonsky,``Quantum Theory
   of Angular Momentum: Irreducible Tensors, Spherical Harmonics, Vector
   Coupling Coefficients, 3nj Symbols'',World Scientific, Singapore, (1988).

	\bibitem{Barrett:2008wh} J.~W.~Barrett and I.~Naish-Guzman,
  ``The Ponzano-Regge model,''
  Class.\ Quant.\ Grav.\  {\bf 26}, 155014 (2009)
  doi:10.1088/0264-9381/26/15/155014
  [arXiv:0803.3319 [gr-qc]].

	\bibitem{Regge:1961px} T.~Regge,
  ``General Relativity Without Coordinates,''
  Nuovo Cim.\  {\bf 19}, 558 (1961).
  doi:10.1007/BF02733251

	\bibitem{Iwasaki:1995vg} J.~Iwasaki,
  ``A Definition of the Ponzano-Regge quantum gravity model in terms of surfaces,''
  J.\ Math.\ Phys.\  {\bf 36}, 6288 (1995)
  doi:10.1063/1.531245
  [gr-qc/9505043].

	\bibitem{TV} V.G.~Turaev and O.Y.~Viro,
   ``State sum invariants of 3 manifolds and quantum 6jsymbols,'' 
    Topology, 31:865–902, 1992.

	\bibitem{Haggard:2009kv} H.~M.~Haggard and R.~G.~Littlejohn,
  ``Asymptotics of the Wigner 9j symbol,''
  Class.\ Quant.\ Grav.\  {\bf 27}, 135010 (2010)
  doi:10.1088/0264-9381/27/13/135010
  [arXiv:0912.5384 [gr-qc]].

	\bibitem{Dona:2017dvf} P.~Dona, M.~Fanizza, G.~Sarno and S.~Speziale,
  ``SU(2) graph invariants, Regge actions and polytopes,''
  Class.\ Quant.\ Grav.\  {\bf 35}, no. 4, 045011 (2018)
  doi:10.1088/1361-6382/aaa53a
  [arXiv:1708.01727 [gr-qc]].
  
    
  \bibitem{Freidel:2004vi} 
  L.~Freidel and D.~Louapre,
  ``Ponzano-Regge model revisited I: Gauge fixing, observables and interacting spinning particles,''
  Class.\ Quant.\ Grav.\  {\bf 21}, 5685 (2004)
  doi:10.1088/0264-9381/21/24/002
  [hep-th/0401076].
  
  \bibitem{Oliveira:2017osu} 
  J.~R.~Oliveira,
  ``EPRL/FK Asymptotics and the Flatness Problem,''
  Class.\ Quant.\ Grav.\  {\bf 35}, no. 9, 095003 (2018)
  doi:10.1088/1361-6382/aaae82
  [arXiv:1704.04817 [gr-qc]].
  
  \bibitem{Bayle:2016doe} 
  V.~Bayle, F.~Collet and C.~Rovelli,
  `'`Short-scale Emergence of Classical Geometry, in euclidean Loop Quantum Gravity,''
  arXiv:1603.07931 [gr-qc].
  
\end{thebibliography}

\end{document}
